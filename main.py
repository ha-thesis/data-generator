import subprocess
import time

if __name__ == "__main__":
    # start generator.py ten times
    subprocesses = []
    for i in range(100):
        subprocesses.append(subprocess.Popen(["py", "generator.py"]))
        time.sleep(0.5)
    try:
       while True:
            time.sleep(1)
            # check if all subprocesses are still running
            if len(subprocesses) == 0:
                break
            for p in subprocesses:
                if p.poll() is not None:
                    print("Process", p.pid, "has terminated")
                    subprocesses.remove(p)

    except KeyboardInterrupt:
      for p in subprocesses:
         p.kill()    
