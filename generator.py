#!/usr/bin/python3
import random
import uuid
import time
import requests
import threading
import json

def generateUUID():
    return str(uuid.uuid4())

def generatePulse(basePulse):
    anomalyChance = random.randint(0,30)
    if anomalyChance == 1 and basePulse < 200:
        print("Anomaly generated!")
        return basePulse + random.randint(30,50)
    newPulse = basePulse + random.randint(-10,10)
    if newPulse < 35:
        return 35
    return newPulse

def sendRequest(uuid, pulse, retries=0):
    if retries > 10:
        return;
    
    time.sleep(retries)

    print("Sending request with UUID:", uuid, "and pulse:", pulse, "retries:", retries)
    myData = {"deviceId": uuid, "pulse": pulse}
    url = "https://data-ingestion-api.c1.papug.cloud/ingest"
    #url = "http://127.0.0.1:8080/ingest"
    headers = {"Content-Type": "application/json"}
    response = requests.post(url, data=json.dumps(myData), headers=headers)

    if response.status_code != 200:
        print("Response:", response.status_code)
        sendRequest(uuid, pulse, retries+1)

    print("Response:", response.status_code)


if __name__ == "__main__":
    random.seed(time.time())
    basePulse = random.randint(40,120)
    endTime = time.time() + 60 * 5 # end in 5 minutes
    myUUID = generateUUID()
    threads = []
    print("Starting generator with UUID:", myUUID)

    while time.time() < endTime:
        basePulse = generatePulse(basePulse)

        t = threading.Thread(target=sendRequest, args=(myUUID, basePulse))
        t.daemon = True
        threads.append(t)
        t.start()

        time.sleep(10)
